import datetime
import scipy
import pandas as pd
import chart_studio.plotly as py
import plotly.graph_objs as go
import cufflinks as cf
import numpy
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot 
init_notebook_mode(connected=True)
cf.go_offline()

date = datetime.date

df = pd.read_csv('https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_19-covid-Confirmed.csv')
cdf = df.groupby('Country/Region').sum()
cdf = cdf.drop(columns=['Lat', 'Long'])  
cdfts = cdf.T
cdfts.index = pd.to_datetime(cdfts.index)


predf = pd.DataFrame({'Mainland China': [41, 41, 41, 45, 62, 198, 275, 291]},
    index=[date(2020, 1, 13), date(2020, 1, 14), date(2020, 1, 15), date(2020, 1, 16),
    date(2020, 1, 17), date(2020, 1, 18), date(2020, 1, 19), date(2020, 1, 20)
    ]
)

cdfts = predf.append(cdfts)
cdfts = cdfts.fillna(0)


cdfdays = cdfts
cdfdays['days'] = pd.factorize(cdfts.index)[0]  
cdfdays = cdfdays.set_index('days')

for col in cdfdays.columns:
    should_cull = cdfdays[col] < 30
    should_cull.drop(should_cull[should_cull == False].index, inplace=True)
    cdfdays[col] = cdfdays[col].shift(periods=-len(should_cull)) 

cdfdays.dropna(axis=1, how='all', inplace=True)

scipy.optimize.curve_fit(lambda t,a,b: a+b*numpy.log(t),  cdfdays.index,  cdfdays['Mainland China'].fillna(0))

cdfdays.to_csv('start_of_infection.csv')
fig = cdfdays.iplot(asFigure=True)
#py.offline.plot(fig)
